# -*- coding: utf-8 -*-
"""
Created on Mon May 28 17:22:49 2018

@author: Hellmer
"""

#discretising datasets
import pandas as pd
import numpy as np
import math

def df_discretiser(df, dfcomp): #inputs: dataframe to discretise, and dataframe to compare to
    df2=pd.DataFrame() 
    def col_average (datafrm, col_name): #creates average from a column
        col_value=datafrm[[col_name]]
        nums=col_value.loc[1:len(col_value)]
        y=np.mean(nums)
        x=float(y)
        return x
    
    collist=list(df.columns)
    for i in collist:
        #takes the average of a column and creates three categories: low, mid and high.
        bins = [dfcomp[i].min(), (col_average(dfcomp, i)/2), col_average(dfcomp, i)+(col_average(dfcomp, i)/2) , dfcomp[i].max()]
        names = ["low", "mid", "high"]
        df2[i]=pd.cut(df[i], bins, labels = names)
    df2.fillna("low", inplace=True)
    return df2
