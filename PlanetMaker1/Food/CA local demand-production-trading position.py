# -*- coding: utf-8 -*-
"""
Created on Sun Jun 17 11:07:48 2018

@author: Nils
"""

import csv
import math

#_______STEP 0___________________________________________________________________________________________________________________
#______________IMPORT DATABASES__________________________________________________________________________________________________
# NEIGHBOUR_LIST (dummy) three triangles: 0,1,2 with their neighbours
neigh_list = [[1, 2, 3, 16, 17, 19, 32, 48, 64, 66, 67], [0, 2, 3, 6, 12, 14, 15, 64, 66, 67, 73, 78], [0, 1, 3, 9, 12, 14, 15, 16, 17, 19, 22, 28]]

#DIET (dummy) = [foodtype,kg of product,resulting_kcal, resulting_demand for agri_m2]
DIET = [['americanbeef',1.06,2502,322.8, 15],['hipstervega',11.52,2499,22.4, 6],['tropicalfruit',4.76,2500,40, 4]]

# GENERAl DATABASE and make lists per triangle per dataset
GD_file = open('General_Database.csv', 'r')
  #reader = csv.reader(GD)
  #GD_list = list(reader)

GD = csv.reader (GD_file)

GH_tr_nr = []
LU_abiotic = []
LU_agri = []
LU_green = []
LU_urban = []
LU_water = []
population = []
rain = []
temp = []

for row in GD:
    GH_tr_nr.append(row[1])
    LU_abiotic.append(row[2])
    LU_agri.append(row[3])
    LU_green.append(row[4])
    LU_urban.append(row[5])
    LU_water.append(row[6])
    population.append(row[8])
    
GD_file.close()

del LU_abiotic [0]
del LU_agri [0]
del LU_green [0]
del LU_urban [0]
del LU_water [0]
del population [0]


turns = 0

#______________POPULATION GROWTH_________________________________________________________________________________________________
def pop_by_year (year, triangle_number):    
    # 0-POPULATION GROWTH (imports tr_pop and transforms it to new_pop which increases according to the turns year)
        # in 2000 the growth rate was 1,33% with a total population of 6.1 billion people
        # The UN-medium-growth prediction is considered the most realistic and estimates a peak of 9.22 billion people in 2075
    if float(population [triangle_number]) == 0:
        loc_pop = 0
        return loc_pop
    else :   
        K = 9220000000
        P0 = 6100000000
        # i had to tweak the maximum growth rate to a higher number in order to match the UN-medium-growth predictions
        r = 0.05
        t = year - 2000   
        part_a = K*P0*math.exp(r*t)
        part_b = K + P0*(math.exp(r*t)-1)
        tot_pop = (part_a /part_b)
        loc_pop_fact = (float(population [triangle_number])/9220000000)
        loc_pop = loc_pop_fact*tot_pop
        return loc_pop

#_______STEP 1___________________________________________________________________________________________________________________
#______________TURN BASED SYSTEM_________________________________________________________________________________________________
while turns < 100:

    turns += 10
    year = 2008 + turns

    if input('press 1 for next decade?') == 1:
        print 'okay there we go'

#_______STEP 2___________________________________________________________________________________________________________________
#______________LOCAL_demand/production/trading position__________________________________________________________________________
    def loc_dem(year, triangle_number, diet_number):
        tr_area = 1280000000000 # total amount of m2 per triangle
        loc_LU = [LU_abiotic [triangle_number], LU_agri [triangle_number], LU_green [triangle_number], LU_urban [triangle_number], LU_water [triangle_number]]
        loc_agri = float(LU_agri [triangle_number])  
        loc_urban = float(LU_urban [triangle_number])
        loc_pop = float(pop_by_year (year ,triangle_number)) # Current population by year per triangle
        
        # define population density and insert if-loop to avoid float division by zero
        if loc_pop == 0 or loc_urban == 0:
            loc_density = 0
        else:
            loc_density = loc_pop/(loc_urban*(tr_area/1000000))
        
        loc_diet = DIET [diet_number]
            
#______________DEMAND____________________________________________________________________________________________________________ 
        # Calculate demand per year (*365) for whole local population
        #loc_dem_tot_kg = loc_diet[1]*365.25*loc_pop # KG IS ONLY NEEDED FOR CALCULATE TRANSPORT-CO2 EMMISSIONS THROUGH TRADE 
        loc_dem_tot_kcal = loc_diet[2]*365.25*loc_pop # local amount of kcal demand needed for calculating hungre
        loc_dem_tot_m2 = loc_diet[3]*365.25*loc_pop # local amount of agri-m2 demand

#______________PRODUCTION________________________________________________________________________________________________________ 
        loc_agri_m2 = loc_agri*tr_area # Calculating the area of agriculture for production
        loc_prod_kcal = loc_agri_m2 * (loc_diet [2]/loc_diet[3]) #Produced kcal = area*(diet_kcal/diet_m2)
        loc_prod_CO2 = loc_agri_m2 * (loc_diet [4]/loc_diet[3]) # Emmission caused by production by area*(diet_CO2/diet_m2)

#______________HUNGRE____________________________________________________________________________________________________________        
        # Calculate amount of hungry people
        energy_req_pp = 2500 #global average factor used for now
        if loc_dem_tot_kcal > loc_prod_kcal:
            loc_pop_hungry = (loc_dem_tot_kcal - loc_prod_kcal)/(energy_req_pp*365.25) # deviding the leftover kcal by the required pp p year
        else:
            loc_pop_hungry = 0
        
#______________TRADE POSITION____________________________________________________________________________________________________
        trade_balance_m2 = loc_agri_m2 - loc_dem_tot_m2 # Save demand/production balance for trading-tool plugin

#______________HUNGRE-EFFECT_____________________________________________________________________________________________________
        # fase 1: if hungre remains for 10 years above thresh: 
            # = decrease green for increase agri 
            # = increase CO2 caused by land change 
            # = decr in CO2 absorb by green area
        # fase 2: if hungre remains for 10 yeas in fase 1 / and green area isn't sufficient: 
            # = ALERT FOR boiling unrest 
            # = start migration
        # fase 3: if hungre remains for 10 years in fase 2: 
            # = start conflict through trading system with neighbour 
            # = decrease in both yields = 2x migration 
            # = temp factr surpressing the total amount of population in conflicted triangles and switch of trading with other triangles
            
#______________REPORT EACH DECADE________________________________________________________________________________________________ 
        print '_______________________________________________'
        print 'TRIANGLE:', triangle_number, 'in', year
        print 'LAND-USE:', loc_LU, '=abiotic,agriculture,green,urban,water'
        print 'DIETTYPE:', loc_diet, '=kg,kcal,m2,CO2'
        print 'POPULATION:', loc_pop
        print 'DENSITY:', loc_density,'pp/km2 urban'
        print '%d people cant eat local' %loc_pop_hungry
        print 'SPATIAL BALANCE:', trade_balance_m2, 'm2'
        print 'CO2e CAUSED',loc_prod_CO2,'gr'

        # Step up to TRADING policy-maker
        if trade_balance_m2 < 0: # if the balance is negative the triangle asks for space
            print '<-------------%d m2 needed' %trade_balance_m2
            return trade_balance_m2
        else:  # if the balance is positive the triangle offers space
            print '------------->%d m2 overproducing' %trade_balance_m2
            return trade_balance_m2
            
#_______STEP 3___________________________________________________________________________________________________________________
#______________USING THE LOCAL FUNCTION__________________________________________________________________________________________
    # iterates over 320 triangles and calculates the local land-use & trading position
    for tr_nr in range (1, 320):
        loc_dem (year, int(tr_nr) , 1)

#______________ENDING THE TURNS__________________________________________________________________________________________________    
else : # ends the while loop for the turn-system
    print 'end of the game'