# -*- coding: utf-8 -*-
"""
Created on Wed May 16 21:32:58 2018

@author: SvanStralen
"""
"""
Purpose of this code is getting data from excel files with lat-lon based data.
So that this data can be assigned to the polyhydron globe and future model.
"""


def data_finder_excelcsv_LonLat(filename,lat,lon):
    import csv
    with open(filename)as csvfile: #open file with data
        reader=csv.reader(csvfile, delimiter=',')
        lon_list = reader.next()[1:]
        lat_list=[]  #create empty list for the lat
        data_row=[]  #this list is used to store all data in row in which data is specified
        for row in reader:
            first_number=row[0]
            lat_list.append(first_number)
            data_row.append(row[1:])
        #print data_row (can test)
        loc_lat=lat_list.index(lat)
        loc_lon=lon_list.index(lon)

        data=data_row[loc_lat][loc_lon]

    return data
#test this shit    
print data_finder_excelcsv_LonLat('MOD11C1_M_LSTDA_2016-01-01_rgb_360x180.SS.CSV','67.5','-179.5')
