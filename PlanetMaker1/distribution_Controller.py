"""
BZZZ ZZZy  ZZZZZZZ  ,ZZZZZZZ  ZZZB ZZZ    ZZZZZz    ZZZ,ZZZj  ZZZZ ZZZ       ZZZ BZZZ  DZZZ,  ZZZZ  ZZZZ
 ZZ   Z5    Zj w 5   wZ   j,   zZ  ZW    ZZ   yZE   DZw  Z9    Z Zw ZW       Zy ZZ     9Z     Z,Zj Z Z
 ZZZZZZz    ZZZZ     jZZZz      8ZZD     ZZ    ZZ   zZj  ZZ    Z 8Z Zy       ZZZZ      ZZ     Z EZ Z Z
 ZZ   ZD    Zy  DZ   WZ   ZB     ZZ      ZZj  ZZD   8ZD  ZZ    Z  Z8Z5       ZD 8Zy    EZ     Z  ZZj Z
8ZZZ ZZZD  ZZZZZZZ  WZZZZZZ9    ZZZZ      jZZZZ      8ZZZZ    ZZZ wZZz       ZZZy9ZZZ  zZZZw  ZZZ ZZ ZZZ

20180627 @Heeyoun Kim
2018 Why Factory_Future Model_Team Energy

###############################################
### DISTRIBUTION ENERGY LOSS FOR  HOUDINI   ###
###############################################

# Distribution controler will return only energy loss from distribution.
# Distributino range should be prepared on the HOUDINI
"""

class distribution_Controller:
    
    def __init__(self):
        ## ENERGY LOSS RATE
        self.ship_en_loss = 0.02
        self.truck_en_loss= 0.03
        self.train_en_loss= 0.04
        self.grid_en_loss= 0.1

    def dist_checker(self, value1, value2, value3):
        dist_pattern = value1
        dist_distance = float(value2)
        dist_energy = float(value3)

        result = 0.0

        #SHIP
        if dist_pattern == 0:
            result = dist_energy * ((1-self.ship_en_loss)**dist_distance)
            print('DISTRIBUTION TYPE: SHIP \n ENERGY LOSS: 2% \n DISTRIBUTED ENERGY:', result)

        #TRUCK
        elif dist_pattern == 1:
            result = dist_energy * ((1-self.truck_en_loss)**dist_distance)
            print('DISTRIBUTION TYPE: TRUCK \n ENERGY LOSS: 3% \n DISTRIBUTED ENERGY:', result)

        #TRAIN
        elif dist_pattern == 2:
            result = dist_energy * ((1-self.train_en_loss)**dist_distance)
            print('DISTRIBUTION TYPE: TRAIN \n ENERGY LOSS: 4% \n DISTRIBUTED ENERGY:', result)

        #GRID
        elif dist_pattern ==3:
            result = dist_energy * ((1-self.grid_en_loss)**dist_distance)
            print('DISTRIBUTION TYPE: GRID \n ENERGY LOSS: 10% \n DISTRIBUTED ENERGY:', result)

        return result




    '''
    def dist_checker(self, value1, value2):
        dist_pattern = value1
        dist_energey = value2
        result = value1

        if dist_pattern == 0:
            result = dist_energey-(dist_energey * self.ship_en_loss)
            print('DISTRIBUTION TYPE: SHIP \n ENERGY LOSS: 2% \n DISTRIBUTED ENERGY:', result )

        elif dist_pattern == 1:
            result = dist_energey-(dist_energey * self.truck_en_loss)
            print('DISTRIBUTION TYPE: TRUCK \n ENERGY LOSS: 3% \n DISTRIBUTED ENERGY:', result )

        if dist_pattern == 2:
            result = dist_energey-(dist_energey * self.train_en_loss)
            print('DISTRIBUTION TYPE: TRAIN \n ENERGY LOSS: 4% \n DISTRIBUTED ENERGY:', result )

        if dist_pattern == 3:
            result = dist_energey-(dist_energey * self.grid_en_loss)
            print('DISTRIBUTION TYPE: GRID \n ENERGY LOSS: 10% \n DISTRIBUTED ENERGY:', result )

        return result
    '''

    #Dist chcker ver 0.1
    """
    def ship_selector(self, value):
        temp_velue = str(value)
        try:
            if temp_velue == 1:
                self.ship_status = 1
                print("you will use ships to distribute energy")
            else:
                self.ship_status = 0
                print("you will not use ships to distribute energy")
        except:
            print('please decide Ship value, on? off?')
            self.ship_status(temp_velue)

    def truck_selector(self, value):
        temp_velue = str(value)
        try:
            if temp_velue == 1:
                self.truck_status = 0
                print("you will use trucks to distribute energy")
            else:
                self.truck_status = 0
                print("you will not use trucks to distribute energy")
        except:
            self.truck_status = 0

    def train_selector(self, value):
        temp_velue = str(value)
        try:
            if temp_velue == 1:
                self.train_status = 1
                print("you will use trains to distribute energy")
            else:
                self.train_status = 0
                print("you will not use trains to distribute energy")

        except:
            self.train_status = 0

    def grid_selector(self, value):
        temp_velue = str(value)
        try:
            if temp_velue == 1:
                self.grid_status = 1
                print("you will use grids to distribute energy")
            else:
                self.grid_status = 0
                print("you will not use grids to distribute energy")
        except:
            self.ship_status = 0
    """



