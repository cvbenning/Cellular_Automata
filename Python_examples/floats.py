#! /usr/bin/env python

a = 10 * 0.999
b = 10 * 0.001
print "a + b: %f" % (a + b)
print "Does a + b equal 10?"
print ((a + b) == 10.0)
print "\n"

f = a + b
for i in range(10):
    f += 0.1

print "f is %f" % f
print "Does f equal a + b + 1.0?"
print (f == (a + b + 1.0))
print "\n"

# this is a better way
if (abs(f - (a + b + 1.0)) < 0.0000001):
    print "f equals a + b + 1.0"
else:
    print "There is a rounding error"

