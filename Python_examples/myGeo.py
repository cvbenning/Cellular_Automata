#! /usr/bin/env python

class myPoint(object):
    def __init__(self, x, y, z):
        self.pt = [x, y, z]

    def __setitem__(self, pos, val):
        self.pt[pos] = val

    def __getitem__(self, pos):
        return self.pt[pos]

    def __add__(self, other):
        result = myPoint(0, 0, 0)
        if isinstance(other, myPoint):
            for i in range(3):
                result[i] = self.pt[i] + other[i]
        return result

    def __sub__(self, other):
        result = myPoint(0, 0, 0)
        if isinstance(other, myPoint):
            for i in range(3):
                result[i] = self.pt[i] - other[i]
        return result


class myTriangle(object):
    def __init__(self, pt0, pt1, pt3, id):
        self.point0 = pt0
        self.point1 = pt1
        self.point2 = pt3
        self.id = id

    def face_normal(self):
        v1 = myPoint(0, 0, 0)
        v2 = myPoint(0, 0, 0)
        v1 = self.point1 - self.point0
        v2 = self.point2 - self.point0
        x = v1[1] * v2[2] - v1[2] * v2[1]
        y = v1[0] * v2[2] - v1[2] * v2[0]
        z = v1[0] * v2[1] - v1[1] * v2[0]
        return myPoint(x, y, z)
        #return v1

    def area(self):
        nrml = myPoint(0, 0, 0)
        nrml = self.face_normal()
        length = nrml[0] * nrml[0] + nrml[1] * nrml[1] + nrml[2] * nrml[2]
        return length * 0.5
